# SNN V2 React Sample App

This project was build as an SPA to consume a simple API and manipulate it's results. With this POC the team can check the viability to use this technology as the main frontend stack.

#### `In the project directory, you can run:`


    yarn install
    yarn start


Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


    yarn test


Launches the test runner in the interactive watch mode.

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


## Implemented features

* Jest for unit tests
* Styled Components for theming
* Redux (Duck Pattern) for state