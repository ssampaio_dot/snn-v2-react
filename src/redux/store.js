import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { saveState, loadState } from 'utils/persistState';
import thunk from 'redux-thunk';
import rootReducer from './ducks';

const logger = createLogger({ collapsed: true });
const middlewares = [thunk];
const persistedState = loadState();

if (process.env.NODE_ENV === 'development') {
  middlewares.push(logger);
}

const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);
const store = createStoreWithMiddleware(rootReducer, persistedState);

store.subscribe(() => {
  saveState(store.getState());
});

export default store;
