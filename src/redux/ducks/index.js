import { combineReducers } from 'redux';

import loginReducer from './login';
import coursesReducer from './courses';

const rootReducer = combineReducers({
  login: loginReducer,
  courses: coursesReducer,
});

export default rootReducer;
