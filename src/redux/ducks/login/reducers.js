import { createAction, createReducer } from '@reduxjs/toolkit';

export const INITIAL_STATE = {
  accessToken: null,
  username: null,
  email: null,
  defaultMessage: null,
  message: null,
  status: null,
  isFetching: false,
};

// actions
export const loginPending = createAction('login/PENDING');
export const loginFulfilled = createAction('login/FULFILLED');
export const loginRejected = createAction('login/REJECTED');
export const loginLogout = createAction('login/LOGOUT');

// reducers
const pending = () => ({ ...INITIAL_STATE, isFetching: true });

const reject = (state, { payload }) => ({
  ...state,
  ...payload,
  isFetching: false,
});

const fulfilled = (state, { payload: { data, user } }) => ({
  ...state,
  accessToken: data.token,
  username: user.username,
  email: user.email,
  isFetching: false,
});

const logout = (state) => ({
  ...state,
  accessToken: null,
});

export default createReducer(INITIAL_STATE, (builder) => {
  builder
    .addCase(loginPending.type, pending)
    .addCase(loginRejected.type, reject)
    .addCase(loginFulfilled.type, fulfilled)
    .addCase(loginLogout.type, logout)
    .addDefaultCase((state) => ({ ...state }));
});
