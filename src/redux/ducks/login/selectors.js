import { createSelector } from 'reselect';

const selectLogin = (state) => state.login;

const selectToken = createSelector(
  [selectLogin],
  ({ accessToken }) => accessToken,
);

const selectFetchingStatus = createSelector(
  [selectLogin],
  ({ isFetching }) => isFetching,
);

const selectUserInfo = createSelector(
  [selectLogin],
  ({ username, email }) => ({
    username,
    email,
  }),
);

const selectError = createSelector(
  [selectLogin],
  ({ status, message, defaultMessage }) => {
    if (status === 'error') {
      return message || defaultMessage;
    }

    return null;
  },
);

export {
  selectToken,
  selectUserInfo,
  selectError,
  selectFetchingStatus,
};
