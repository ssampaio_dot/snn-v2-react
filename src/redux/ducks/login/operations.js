import fetchData from 'services/fetchData';
import * as Login from './reducers';

const doLogin = ({ email, password }) => async (dispatch) => {
  const requestObj = {
    method: 'post',
    url: '/login',
    data: {
      email,
      password,
    },
  };

  // pending action
  dispatch(Login.loginPending());

  const [response, error] = await fetchData(requestObj);

  if (error) {
    return dispatch(Login.loginRejected(error));
  }

  // fulfilled data
  return dispatch(Login.loginFulfilled(response));
};

const doLogout = () => (dispatch) => {
  dispatch(Login.loginLogout());
};

export {
  doLogin,
  doLogout,
};
