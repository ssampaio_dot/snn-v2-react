import store from 'redux/store';
import { doLogin, doLogout } from '../operations';
import fulfilledMock from '../__mocks__/loginFulfilled.json';

describe('LOGIN OPERATIONS', () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  describe('doLogin', () => {
    it('should add login credentials in state', async () => {
      fetch.mockResponseOnce(JSON.stringify(fulfilledMock));

      await store.dispatch(doLogin({ email: 'email', password: 'password' }));
      const state = store.getState().login;

      expect(state.accessToken).toEqual(fulfilledMock.data.token);
      expect(state.username).toEqual(fulfilledMock.user.username);
      expect(state.email).toEqual(fulfilledMock.user.email);
    });

    it('should add error data in state', async () => {
      const error = 'Error trying to login';
      fetch.mockReject(new Error(error));

      await store.dispatch(doLogin({ email: 'email', password: 'password' }));
      const state = store.getState().login;

      expect(state.status).toEqual('error');
      expect(state.message).toEqual(error);
    });
  });

  describe('doLogout', () => {
    it('should clear credentials in state', async () => {
      let state;
      fetch.mockResponseOnce(JSON.stringify(fulfilledMock));

      await store.dispatch(doLogin({ email: 'email', password: 'password' }));
      state = store.getState().login;
      expect(state.accessToken).toEqual(fulfilledMock.data.token);

      await store.dispatch(doLogout());
      state = store.getState().login;
      expect(state.accessToken).toBeNull();
    });
  });
});
