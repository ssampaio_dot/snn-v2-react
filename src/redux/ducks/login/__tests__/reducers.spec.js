import * as Login from '../reducers';
import reducer from '../reducers';

describe('LOGIN REDUCERS', () => {
  describe('Action Types', () => {
    it('should return the login/PENDING action type', () => {
      expect(Login.loginPending.type).toEqual('login/PENDING');
    });

    it('should return the login/FULFILED action type', () => {
      expect(Login.loginFulfilled.type).toEqual('login/FULFILLED');
    });

    it('should return the login/REJECTED action type', () => {
      expect(Login.loginRejected.type).toEqual('login/REJECTED');
    });
  });

  describe('Action creators', () => {
    it('should return the loginPending action creator', () => {
      expect(Login.loginPending()).toHaveProperty('type');
      expect(Login.loginPending().type).toEqual('login/PENDING');
    });

    it('should return the loginFulfilled action creator', () => {
      expect(Login.loginFulfilled()).toHaveProperty('type');
      expect(Login.loginFulfilled().type).toEqual('login/FULFILLED');
    });

    it('should return the loginRejected action creator', () => {
      expect(Login.loginRejected()).toHaveProperty('type');
      expect(Login.loginRejected().type).toEqual('login/REJECTED');
    });
  });

  describe('Reducers', () => {
    it('should reset the state object and set isFetching to true', () => {
      const state = reducer({
        status: 'error',
        message: 'error message',
      }, { type: Login.loginPending.type });

      expect(state.status).toBeNull();
      expect(state.message).toBeNull();
      expect(state.isFetching).toBeTruthy();
    });

    it('should set isFetching to false and fill error properties with payload data', () => {
      let state = Login.INITIAL_STATE;

      state = reducer(state, { type: Login.loginPending.type });
      state = reducer(state, {
        type: Login.loginRejected.type,
        payload: {
          defaultMessage: 'default message',
          message: 'error message',
          status: 'error',
        },
      });

      expect(state.defaultMessage).toEqual('default message');
      expect(state.message).toEqual('error message');
      expect(state.status).toEqual('error');
      expect(state.isFetching).toBeFalsy();
    });

    it('should set isFetching to false and fill user access properties with payload data', () => {
      let state = Login.INITIAL_STATE;

      state = reducer(state, { type: Login.loginPending.type });
      state = reducer(state, {
        type: Login.loginFulfilled.type,
        payload: {
          data: {
            token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9',
          },
          user: {
            username: 'dot_user',
            email: 'user@dotgroup.com.br',
          },
        },
      });

      expect(state.accessToken).toEqual('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9');
      expect(state.username).toEqual('dot_user');
      expect(state.email).toEqual('user@dotgroup.com.br');
      expect(state.isFetching).toBeFalsy();
    });
  });
});
