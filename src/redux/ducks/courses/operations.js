import fetchData from 'services/fetchData';
import * as Courses from './reducers';

/**
 * Get courses lits
 */
const getCourses = () => async (dispatch) => {
  const requestObj = {
    method: 'post',
    url: '/graphql',
    data: {
      query: `{
        Course(searchParam: {
          pagination: {
            page: 1, 
            perPage: 20,
          }
        }) {
          rows {
            id
            name
          }
        }
      }`,
    },
  };

  // pending action
  dispatch(Courses.coursesPending());

  const [response, error] = await fetchData(requestObj);

  if (error) {
    return dispatch(Courses.coursesRejected(error));
  }

  // fulfilled data
  const courses = response.data.Course.rows;
  return dispatch(Courses.coursesFulfilled({ courses }));
};

/**
 * Create a new course
 * @param {string} courseName The course name
 */
const addCourse = (courseName) => async (dispatch) => {
  const requestObj = {
    method: 'post',
    url: '/graphql',
    data: {
      query: `mutation {
        upsertCourse(input: {
          name: "${courseName}",
        }) {
          id
          name
        }
      }`,
    },
  };

  const [response, error] = await fetchData(requestObj);

  if (error) {
    return dispatch(Courses.coursesRejected(error));
  }

  // fulfilled data
  const addedCourse = response.data.upsertCourse;
  return dispatch(Courses.coursesUpsert(addedCourse));
};

/**
 * Update an existing course
 * @param {string} name The course name
 * @param {number} id The course id
 */
const updateCourse = ({ name, id }) => async (dispatch) => {
  const requestObj = {
    method: 'post',
    url: '/graphql',
    data: {
      query: `mutation {
        upsertCourse(input: {
          name: "${name}",
          id: ${id},
        }) {
          id
          name
        }
      }`,
    },
  };

  const [response, error] = await fetchData(requestObj);

  if (error) {
    return dispatch(Courses.coursesRejected(error));
  }

  // fulfilled data
  const updatedCourse = response.data.upsertCourse;
  return dispatch(Courses.coursesUpsert(updatedCourse));
};

export {
  getCourses,
  addCourse,
  updateCourse,
};
