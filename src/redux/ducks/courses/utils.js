/**
 * Verifies if course should be inserted or updated in the current array
 * @param {array} courses Array of courses
 * @param {object} courseToCheck The object to be checkd
 */
function checkUpsert(courses, courseToCheck) {
  const itemExists = courses.find((course) => course.id === courseToCheck.id);

  if (itemExists) {
    return courses.map((course) => {
      if (course.id === courseToCheck.id) {
        return {
          ...course,
          name: courseToCheck.name,
        };
      }

      return course;
    });
  }

  return [
    ...courses,
    courseToCheck,
  ];
}

export {
  checkUpsert,
};
