import { createAction, createReducer } from '@reduxjs/toolkit';
import { checkUpsert } from './utils';

export const INITIAL_STATE = {
  courses: [],
  defaultMessage: null,
  message: null,
  status: null,
  isFetching: false,
};

// actions
export const coursesPending = createAction('courses/PENDING');
export const coursesFulfilled = createAction('courses/FULFILLED');
export const coursesRejected = createAction('courses/REJECTED');
export const coursesUpsert = createAction('courses/UPSERT');

// reducers
const pending = () => ({ ...INITIAL_STATE, isFetching: true });

const reject = (state, { payload }) => ({
  ...state,
  ...payload,
  isFetching: false,
});

const fulfilled = (state, { payload: { courses } }) => ({
  ...state,
  courses,
  isFetching: false,
});

const upsert = (state, { payload }) => ({
  ...state,
  courses: checkUpsert(state.courses, payload),
});

export default createReducer(INITIAL_STATE, (builder) => {
  builder
    .addCase(coursesPending.type, pending)
    .addCase(coursesRejected.type, reject)
    .addCase(coursesFulfilled.type, fulfilled)
    .addCase(coursesUpsert.type, upsert)
    .addDefaultCase((state) => ({ ...state }));
});
