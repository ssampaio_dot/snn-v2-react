import { createSelector } from 'reselect';

const selectCourses = (state) => state.courses;

const selectAllCourses = createSelector(
  [selectCourses],
  ({ courses }) => courses,
);

const selectCourse = (courseId) => createSelector(
  [selectAllCourses],
  (courses) => courses.find((course) => course.id === Number(courseId)),
);

const selectFetchingStatus = createSelector(
  [selectCourses],
  ({ isFetching }) => isFetching,
);

const selectError = createSelector(
  [selectCourses],
  ({ status, message, defaultMessage }) => {
    if (status === 'error') {
      return message || defaultMessage;
    }

    return null;
  },
);

export {
  selectAllCourses,
  selectError,
  selectFetchingStatus,
  selectCourse,
};
