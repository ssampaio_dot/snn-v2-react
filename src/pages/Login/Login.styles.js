import styled from 'styled-components';

const Login = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem;
`;

const Title = styled.h1`
  font-size: ${({ theme: { fontSizes } }) => fontSizes.large};
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  padding-top: 4rem;
  width: fit-content;
`;

const InputContainer = styled.label`
  display: flex;
  flex-direction: column;
  margin-bottom: 2rem;

  &:last-of-type {
    margin-bottom: 0;
  }

  span {
    text-transform: uppercase;
  }

  input {
    border: 0;
    border-bottom: 1px solid #000000;
    font-size: ${({ theme: { fontSizes } }) => fontSizes.medium};
    padding: 0.5rem 0;
    width: 35rem;
  }
`;

const Button = styled.button`
  ${({ theme: { primaryButton } }) => primaryButton};
  margin-top: 2rem;
`;

const ErrorContainer = styled.div`
  color: red;
  padding: 0.6rem 0;
  
  span {
    font-size: 1.1rem;
  }
`;

export {
  Login,
  Title,
  Form,
  InputContainer,
  Button,
  ErrorContainer,
};
