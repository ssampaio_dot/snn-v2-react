import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { doLogin } from 'redux/ducks/login/operations';
import { selectError, selectToken, selectFetchingStatus } from 'redux/ducks/login/selectors';
import * as Styled from './Login.styles';

function Login({
  history, error, token, isLoading,
}) {
  const dispatch = useDispatch();
  const [formData, setFormData] = useState({
    validationError: null,
    email: '',
    password: '',
  });

  useEffect(() => {
    if (token) {
      history.push('/courses');
    }
  }, [token, history]);

  function handleLogin() {
    if (formData.email && formData.password) {
      return dispatch(doLogin(formData));
    }

    return setFormData((oldState) => ({
      ...oldState,
      validationError: 'Todos os campos são obrigatórios',
    }));
  }

  function handleChange(evt) {
    const { name, value } = evt.target;

    setFormData((oldState) => ({
      ...oldState,
      validationError: null,
      [name]: value.trim(),
    }));
  }

  return (
    <Styled.Login>
      <Styled.Title>Login Page</Styled.Title>
      <Styled.Form>
        <Styled.InputContainer htmlFor="email">
          <span>E-mail</span>
          <input
            onChange={(evt) => handleChange(evt)}
            name="email"
            type="email"
            required
            autoComplete="off"
            value={formData.email}
          />
        </Styled.InputContainer>
        <Styled.InputContainer htmlFor="password">
          <span>Password</span>
          <input
            onChange={(evt) => handleChange(evt)}
            name="password"
            type="password"
            required
            value={formData.password}
          />
        </Styled.InputContainer>
        <Styled.Button type="button" onClick={handleLogin}>{ !isLoading ? 'Login' : '...Aguarde' }</Styled.Button>

        <Styled.ErrorContainer>
          <span>{formData.validationError}</span>
          <span>{error}</span>
        </Styled.ErrorContainer>
      </Styled.Form>
    </Styled.Login>
  );
}

Login.propTypes = {
  history: PropTypes.shape().isRequired,
  error: PropTypes.string,
  token: PropTypes.string,
  isLoading: PropTypes.bool.isRequired,
};

Login.defaultProps = {
  error: null,
  token: null,
};

const mapStateToProps = createStructuredSelector({
  error: selectError,
  token: selectToken,
  isLoading: selectFetchingStatus,
});

export default connect(
  mapStateToProps,
)(Login);
