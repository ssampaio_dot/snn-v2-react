import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { getCourses, addCourse, updateCourse } from 'redux/ducks/courses/operations';
import { doLogout } from 'redux/ducks/login/operations';
import { selectError, selectFetchingStatus, selectAllCourses } from 'redux/ducks/courses/selectors';

import CourseList from 'components/CourseList';
import CourseAdd from 'components/CourseAdd';
import CourseInfo from 'components/CourseInfo';

import * as Styled from './Courses.styles';

function Courses({
  history, courses, error, isLoading,
}) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCourses());
  }, [dispatch]);

  function handleEdit(course) {
    dispatch(updateCourse(course));
  }

  function handleAddCourse(courseName) {
    dispatch(addCourse(courseName));
  }

  function handleLogout() {
    dispatch(doLogout());
  }

  function handleView(courseId) {
    history.push(`/courses/${courseId}`);
  }

  return (
    <Styled.Courses>
      <Styled.LogoutButton type="button" onClick={handleLogout}>Logout</Styled.LogoutButton>
      <Styled.Title>COURSES COMPONENT</Styled.Title>

      { isLoading ? <span>Carregando...</span> : (
        <CourseList onView={handleView} onEdit={handleEdit} data={courses} />
      ) }

      <CourseAdd onAdd={handleAddCourse} />
      <span>{error}</span>

      <Route path="/courses/:id" component={CourseInfo} />
    </Styled.Courses>
  );
}

Courses.propTypes = {
  error: PropTypes.string,
  courses: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    }),
  ).isRequired,
  isLoading: PropTypes.bool.isRequired,
  history: PropTypes.shape().isRequired,
};

Courses.defaultProps = {
  error: null,
};

const mapStateToProps = createStructuredSelector({
  error: selectError,
  courses: selectAllCourses,
  isLoading: selectFetchingStatus,
});

export default connect(
  mapStateToProps,
)(Courses);
