import styled from 'styled-components';

const Courses = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem;
`;

const Title = styled.h1`
  font-size: ${({ theme: { fontSizes } }) => fontSizes.large};
`;

const LogoutButton = styled.button`
  width: fit-content;
  text-decoration: underline;
  border: none;
  background-color: transparent;
  font-size: ${({ theme }) => theme.fontSizes.medium};
  margin-bottom: 4rem;
`;

export {
  Courses,
  Title,
  LogoutButton,
};
