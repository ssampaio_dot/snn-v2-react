import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import PrivateRoute from 'containers/PrivateRoute';
import Login from './Login';
import Courses from './Courses';

export default () => (
  <Switch>
    <Route path="/login" component={Login} />
    <PrivateRoute path="/courses" component={Courses} />
    <Redirect to="/courses" />
  </Switch>
);
