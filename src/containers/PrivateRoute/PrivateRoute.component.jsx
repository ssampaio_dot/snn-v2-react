/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import { withRouter, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { selectToken } from 'redux/ducks/login/selectors';

const filterAccess = ({
  accessToken,
  component: Component,
  ...otherProps
}) => (
  accessToken
    ? <Component {...otherProps} />
    : (
      <Redirect to={{
        pathname: '/login',
        state: { from: otherProps.location },
      }}
      />
    )
);

filterAccess.propTypes = {
  accessToken: PropTypes.string.isRequired,
  component: PropTypes.shape().isRequired,
};

const PrivateRoute = (props) => (
  <Route render={() => filterAccess(props)} />
);

const mapStateToProps = createStructuredSelector({
  accessToken: selectToken,
});

export default withRouter(connect(mapStateToProps)(PrivateRoute));
