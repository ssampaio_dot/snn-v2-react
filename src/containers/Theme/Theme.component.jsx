import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';

const theme = {
  colors: {
    titleColor: '#333333',
  },
  primaryFont: 'Roboto',
  fontSizes: {
    small: '1rem',
    medium: '2rem',
    large: '3rem',
  },
  primaryButton: {
    backgroundColor: '#000000',
    border: 'none',
    color: '#ffffff',
    cursor: 'pointer',
    padding: '1rem 0.5rem',
    textTransform: 'uppercase',
  },
};

const Theme = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

Theme.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Theme;
