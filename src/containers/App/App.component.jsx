import React from 'react';
import Pages from 'pages';
import Theme from 'containers/Theme';

function App() {
  return (
    <Theme>
      <Pages />
    </Theme>
  );
}

export default App;
