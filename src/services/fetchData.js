import store from 'redux/store';

async function fetchData({
  url, method = 'get', data = null, ...props
}) {
  const state = store.getState();
  const apiHost = process.env.REACT_APP_API_URL;
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  let queryParams = '';

  let fetchConfig = {
    headers,
    method,
    ...props,
  };

  if (state.login.accessToken) {
    headers.Authorization = `Bearer ${state.login.accessToken}`;
  }

  if (method !== 'get' && data) {
    fetchConfig = {
      ...fetchConfig,
      body: JSON.stringify(data),
    };
  }

  if (method === 'get' && data) {
    queryParams += '?';

    Object
      .entries(data)
      .forEach(([key, value]) => {
        queryParams += `${key}=${value}&`;
      });

    queryParams = queryParams.substring(0, queryParams.length - 1);
  }

  try {
    const response = await fetch(`${apiHost}${url}${queryParams}`, fetchConfig);
    const responseData = await response.json();

    if (!response.ok) {
      throw new Error(JSON.stringify({
        ...responseData,
        defaultMessage: 'Houve um erro ao tentar consumir a API',
      }));
    }

    return ([responseData, null]);
  } catch (error) {
    let errorResponse;
    try {
      errorResponse = JSON.parse(error.message);
    } catch (e) {
      errorResponse = {
        message: error.message,
        status: 'error',
      };
    }
    return ([null, errorResponse]);
  }
}

export default fetchData;
