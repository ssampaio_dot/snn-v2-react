import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as Styled from './CourseAdd.styles';

function CourseAdd({ onAdd }) {
  const [name, setName] = useState('');
  const [addMode, setAddMode] = useState(false);

  function handleChange(evt) {
    setName(evt.target.value);
  }

  function toggleMode() {
    setAddMode(!addMode);
  }

  function handleAdd() {
    if (name.trim() !== '') {
      onAdd(name.trim());
      setName('');
      toggleMode();
    }
  }

  return (
    <Styled.CourseAdd>
      { addMode ? (
        <>
          <Styled.AddField type="text" onChange={(evt) => handleChange(evt)} />
          <Styled.Button type="button" onClick={toggleMode}>Cancel</Styled.Button>
          <Styled.Button type="button" onClick={handleAdd}>Add</Styled.Button>
        </>
      ) : (
        <Styled.Button type="button" onClick={toggleMode}>New</Styled.Button>
      )}
    </Styled.CourseAdd>
  );
}

CourseAdd.propTypes = {
  onAdd: PropTypes.func.isRequired,
};

export default CourseAdd;
