import styled from 'styled-components';

const CourseAdd = styled.div`
  display: flex;
  align-items: center;
  width: fit-content;
`;

const AddField = styled.input`
  border: 1px solid #000000;
  font-size: ${({ theme }) => theme.fontSizes.medium};
  min-width: 35rem;
  padding: 0.3rem 0.5rem;
`;

const Button = styled.button`
  ${({ theme }) => theme.primaryButton};
  margin-left: 1rem;
  padding-left: 1rem;
  padding-right: 1rem;
  border-radius: 0.3rem;
  min-width: 10rem;
`;

export {
  CourseAdd,
  AddField,
  Button,
};
