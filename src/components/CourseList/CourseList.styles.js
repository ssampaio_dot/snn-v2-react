import styled from 'styled-components';

const CourseList = styled.div`
  display: flex;
  flex-direction: column;
  width: 30rem;
  padding: 2rem 0;

  button {
    margin-bottom: 1rem;
  }
`;

export {
  CourseList,
};
