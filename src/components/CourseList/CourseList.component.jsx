import React from 'react';
import PropTypes from 'prop-types';

import CourseItem from 'components/CourseItem';
import * as Styled from './CourseList.styles';

function CourseList({ data: courses, onEdit, onView }) {
  return (
    <Styled.CourseList>
      { courses.length > 0 ? courses.map((course) => (
        <CourseItem onView={onView} onEdit={onEdit} key={course.id} data={course} />
      )) : null }
    </Styled.CourseList>
  );
}

CourseList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    }),
  ).isRequired,
  onEdit: PropTypes.func.isRequired,
  onView: PropTypes.func.isRequired,
};

export default CourseList;
