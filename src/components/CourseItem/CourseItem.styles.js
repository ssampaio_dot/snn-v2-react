import styled, { css } from 'styled-components';
import { switchProp } from 'styled-tools';

const CourseItem = styled.div`
  align-items: center;
  display: flex;
  margin-bottom: 1rem;
`;

const CourseName = styled.input`
  border-radius: 0.2rem;
  font-size: ${({ theme }) => theme.fontSizes.medium};
  min-width: 35rem;
  padding: 0.3rem 0.5rem;

  ${switchProp('editable', {
    true: css`
      cursor: initial;
      border-bottom: 1px solid #000000;
      border-top: 1px solid #000000;
      border-right: 1px solid #000000;
      border-left: 1px solid #000000;
    `,
    false: css`
      cursor: auto;
      border-bottom: 1px solid #00000033;
      border-top: 1px solid #00000000;
      border-right: 1px solid #00000000;
      border-left: 1px solid #00000000;
    `,
  })}
`;

const Button = styled.button`
  ${({ theme: { primaryButton } }) => primaryButton};
  border-radius: 3px;
  margin: 0 0 0 1rem !important;
  min-width: 10rem;
`;

export {
  CourseItem,
  CourseName,
  Button,
};
