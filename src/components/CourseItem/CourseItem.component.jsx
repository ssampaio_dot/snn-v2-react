import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import * as Styled from './CourseItem.styles';

function CourseItem({ onEdit, onView, data }) {
  const courseInput = useRef(null);
  const [editMode, setEditMode] = useState(false);
  const [name, setName] = useState('');

  useEffect(() => {
    if (data.name) {
      setName(data.name);
    }
  }, [data]);

  function handleChange(evt) {
    setName(evt.target.value);
  }

  function toggleEdit() {
    setEditMode(!editMode);

    if (courseInput.current.readOnly) {
      courseInput.current.select();
    }
  }

  function handleSave() {
    if (name.trim() !== '') {
      onEdit({ name: name.trim(), id: data.id });
      toggleEdit();
    } else {
      setName(data.name);
    }
  }

  function handleView() {
    onView(data.id);
  }

  return (
    <Styled.CourseItem>
      <Styled.CourseName ref={courseInput} readOnly={!editMode} editable={editMode} type="text" value={name} onChange={(evt) => handleChange(evt)} />
      { !editMode ? (
        <Styled.Button type="button" onClick={toggleEdit}>Edit</Styled.Button>
      ) : (
        <Styled.Button type="button" onClick={handleSave}>Save</Styled.Button>
      )}
      <Styled.Button type="button" onClick={handleView}>Details</Styled.Button>
    </Styled.CourseItem>
  );
}

CourseItem.propTypes = {
  data: PropTypes.shape().isRequired,
  onEdit: PropTypes.func.isRequired,
  onView: PropTypes.func.isRequired,
};

export default CourseItem;
