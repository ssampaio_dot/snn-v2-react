import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { selectCourse } from 'redux/ducks/courses/selectors';
import * as Styled from './CourseInfo.styles';

function CourseInfo({ history, course }) {
  function closeInfo() {
    history.push('/courses');
  }

  return (
    <Styled.CourseInfo>
      { course ? (
        <Styled.DataWrapper>
          <Styled.Title>Course Info</Styled.Title>

          <Styled.SubTitle>Sample modal to show course info</Styled.SubTitle>

          <Styled.InfoWrapper>
            <span>
              <strong>Id:</strong>
              {course.id}
            </span>
            <span>
              <strong>Name:</strong>
              {course.name}
            </span>
          </Styled.InfoWrapper>

          <Styled.Button type="button" onClick={closeInfo}>Close</Styled.Button>
        </Styled.DataWrapper>
      ) : null }
    </Styled.CourseInfo>
  );
}

CourseInfo.propTypes = {
  history: PropTypes.shape().isRequired,
  course: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  }),
};

CourseInfo.defaultProps = {
  course: null,
};

const mapStateToProps = createStructuredSelector({
  course: (state, props) => selectCourse(props.match.params.id)(state),
});

export default connect(
  mapStateToProps,
)(CourseInfo);
