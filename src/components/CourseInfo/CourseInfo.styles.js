import styled from 'styled-components';

const CourseInfo = styled.div`
  backdrop-filter: blur(1px) brightness(80%);
  left: 0;
  height: 100vh;
  position: absolute;
  top: 0;
  width: 100vw;
  z-index: 1;
`;

const DataWrapper = styled.div`
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  min-width: 40rem;
  margin: 3rem;
  padding: 2rem;
  width: fit-content;
`;

const Title = styled.h1`
  font-size: ${({ theme }) => theme.fontSizes.large};
`;

const SubTitle = styled.h2`
  color: #33333388;
  font-size: 1rem;
  font-weight: 400;
  text-transform: uppercase;
  margin-top: 0rem;
`;

const InfoWrapper = styled.p`
  display: flex;
  flex-direction: column;
  padding: 1rem 0 1.5rem;
  margin: 2rem 0 2.5rem;
  border-top: 1px solid #33333355;
  border-bottom: 1px solid #33333355;
  
  span {
    color: #000000;
    font-size: ${({ theme }) => theme.fontSizes.medium} !important;
  }
  
  strong {
    display: inline-block;
    color: #33333388;
    font-size: 1.4rem;
    text-transform: uppercase;
    margin-right: 1rem;
    min-width: 4.5rem;
  }
`;

const Button = styled.button`
  ${({ theme }) => theme.primaryButton};
  padding-left: 1rem;
  padding-right: 1rem;
  border-radius: 0.3rem;
  min-width: 10rem;
`;

export {
  CourseInfo,
  DataWrapper,
  Title,
  SubTitle,
  InfoWrapper,
  Button,
};
