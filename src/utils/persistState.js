const PERSIST_KEY = 'persist:snn';
const WHITELIST = ['login'];

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem(PERSIST_KEY);

    if (serializedState === null) {
      return undefined;
    }

    return JSON.parse(serializedState);
  } catch (error) {
    return undefined;
  }
};

export const saveState = (state) => {
  try {
    const filtered = Object.keys(state)
      .filter((key) => WHITELIST.includes(key))
      .reduce((obj, key) => ({
        ...obj,
        [key]: state[key],
      }), {});

    const serializedState = JSON.stringify(filtered);
    localStorage.setItem(PERSIST_KEY, serializedState);
  } catch (error) {
    // nothing to do here
  }
};
